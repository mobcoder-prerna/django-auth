from django.contrib.auth import password_validation, get_user_model
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.password_validation import validate_password
from django.core.validators import RegexValidator
from rest_framework import serializers

from .models import CustomUser

User = get_user_model()


class UserRegisterSerializer(serializers.ModelSerializer):
    """
    A user serializer for registering the user
    """

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'password', 'full_name', 'phone','user_type')
        extra_kwargs = {'password': {'write_only': True}}


    def validate_password(self, value):
        password_validation.validate_password(value)
        return value

    def create(self, validated_data):

        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserLoginserializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ['username','password']



class UpdateSerializer(serializers.Serializer):
    username           = serializers.CharField(max_length=200,required=False)
    email              = serializers.CharField(max_length=256,required=False)
    full_name          = serializers.CharField(max_length=256,required=False)
    phone_regex        = RegexValidator(regex=r'^\d{10,15}$')
    phone              = serializers.CharField(max_length=15,required=False)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.full_name = validated_data.get('full_name', instance.full_name)
        instance.phone=validated_data.get('phone', instance.phone)
        instance.save()
        return(instance)