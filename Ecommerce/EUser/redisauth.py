import redis

# Settings
r = redis.Redis(host='localhost', port=6379, db=0)


def setRedis(key, value, ex=43200):
	try:
		val = r.set(key,value)
		print(val,"valllllllllllllllll")
		return(val)
	except Exception as e:
		print(e,"error")
		return(e)


def setExpiry(key):
	try:
		r.delete(key)
	except Exception as e:
		print(e)
		return(e)


def getValue(key):
	try:
		value = r.get(key)
		return value
	except Exception as e:
		print(e,"error")
		return(e)
