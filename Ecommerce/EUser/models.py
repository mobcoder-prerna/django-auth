from django.core.validators import RegexValidator
from djongo import models
from django.contrib.auth.models import (BaseUserManager, PermissionsMixin, AbstractUser)

# Create your models here.

class UserManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if username is None:
            raise TypeError('User should have a username')
        if email is None:
            raise TypeError('User should have email')

        user = self.model(username=username, email=self.normalize_email(email))
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password):
        if password is None:
            raise TypeError('Password should not be none')

        user = self.create_user(username, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user




class CustomUser(AbstractUser):
    USER_TYPES = (
        ('S', 'Seller'),
        ('B', 'Buyer'),
    )
    id           =    models.BigAutoField(auto_created=True, primary_key=True, serialize=True, verbose_name='ID')
    username     = models.CharField('username', unique=True, max_length=255, blank=False, null=False)
    email        = models.EmailField('email address', max_length=255, blank=False,unique=True)
    full_name    = models.CharField('Full Name', max_length=255, blank=True,
                                 null=False)
    phone_regex  = RegexValidator(regex=r'[+]\d{10,12}$', message="Phone number must be entered in the format:"
                                                              " '+999999999'. Up to 12 digits allowed.")
    phone        = models.CharField(validators=[phone_regex], max_length=12, blank=True)
    user_type    = models.CharField(max_length=1, choices=USER_TYPES, default='S')
    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.username

    # objects = models.DjongoManager()


