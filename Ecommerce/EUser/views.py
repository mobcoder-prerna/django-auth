import re
from urllib.error import HTTPError
import jwt
from django.conf import settings
from django.contrib.auth import authenticate
from rest_framework import generics
from rest_framework.authentication import BasicAuthentication
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.serializers import jwt_payload_handler, JSONWebTokenSerializer
from .redisauth import setRedis, getValue, setExpiry
from .serializers import *
from .utils import conditional_exception_handler

User = get_user_model()

def decode_token(token):
    valid_data = jwt.decode(token,settings.SECRET_KEY)
    User = valid_data
    u_id = User['user_id']
    return u_id


class Registration(generics.CreateAPIView):
    # authentication_classes = [BasicAuthentication]
    parser_classes = (FormParser, MultiPartParser)
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRegisterSerializer


class UserLogin(generics.CreateAPIView):
    # authentication_classes = [BasicAuthentication]
    parser_classes = (FormParser, MultiPartParser)
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, ]
    serializer_class = UserLoginserializer

    def post(self, request, *args, **kwargs):
        try:
            username = self.request.data['username']
            password = self.request.data['password']
            queryset = User.objects.filter(username=username).values('password')
            user = authenticate(username=username, password=password)
            if len(queryset) == 0:
                custom_error = conditional_exception_handler("User with this username not found")
                return Response(custom_error,status=400)
            if user is None:
                custom_error = conditional_exception_handler("You have entered the wrong password")
                return Response(custom_error,status=400)
            user = User.objects.get(username=user)
            payload = jwt_payload_handler(user)
            # payload = {"username":user.username,"phone":user.phone}
            token = jwt.encode(payload, settings.SECRET_KEY)
            setRedis(username, token)
            return Response({'msg': 'Login Successfully', 'access_token': token}, status=200)
        except Exception as e:
            custom_error = conditional_exception_handler(str(e))
            return Response(custom_error,status=400)



class Logout(generics.GenericAPIView):
    parser_classes = (FormParser, MultiPartParser)
    serializer_class = JSONWebTokenSerializer

    def get(self, request, *args, **kwargs):
        try:
            token = request.META.get('HTTP_AUTHORIZATION')
            if token:
                token = token.split(' ')[1]
                uid = decode_token(token)
                user = User.objects.get(id = uid).username
                if user:
                    print(user,"user found")
                    if getValue(user) == None:
                        return Response({'msg': 'Session Expired login first'}, status=400)
                    setExpiry(user)
                return Response({'msg': 'Logout Successfully'}, status=200)
            else:
                custom_error = conditional_exception_handler(str("please enter valid token"))
                return Response(custom_error,status=400)
        except Exception as e:
            custom_error = conditional_exception_handler(str(e))
            return Response(custom_error,status=400)




class UpdateProfile(generics.GenericAPIView):
    parser_classes = (FormParser, MultiPartParser)
    permission_classes = (IsAuthenticated,)
    serializer_class= UpdateSerializer

    def put(self,request, *args, **kwargs):
        # print(request.data,"request data for update")
        try:
            token = request.META.get('HTTP_AUTHORIZATION')
            if token:
                token = token.split(' ')[1]
                uid = decode_token(token)
                serializer = self.get_serializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                instance = CustomUser.objects.get(id = uid)
                getValue(instance.username)
                print(getValue)
                UpdateSerializer.update(self, instance=instance, validated_data=request.data)
                return Response({'status':1,'responseData':{'message':"User updated Successfully"}},status=200)
            else:
                custom_error = conditional_exception_handler(str("please enter valid token"))
                return Response(custom_error,status=400)
        except Exception as e:
            print(e)
            if 'detail' in e.__dict__.keys():
                d =list(e.__dict__['detail'].keys())[0]
                message = e.__dict__['detail'][d][0]
                # print(message)
            else:
                message = str(e)
            custom_error = conditional_exception_handler(message)
            return Response(custom_error,status=400)