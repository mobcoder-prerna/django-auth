from django.urls import path
from .views import *

urlpatterns = [
    path('registration/',Registration.as_view()),
    path('login/', UserLogin.as_view()),
    path('updateProfile/',UpdateProfile.as_view()),
    path('logout/',Logout.as_view())
]